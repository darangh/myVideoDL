31/10/2017
myVideoDL est une interface à youtube-dl, programmée sous QtCreator. Le but est de sélectionner facilement l'une des 3 tailles de videos les plus répandues pour téléchargement. 
Il est complémentaire à VideoDownloadHelper, le plugin de Firefox. Bien sûr, il est beaucoup moins complet en terme d'Urls gérés, mais vise les formats (qui me sont personnellement) les plus utiles et surtout, utilise beaucoup moins de ressources que VideoDownloadHelper, puisque c'est une appli compilée, et totalement séparée de Firefox !

---------------------------------------------------------------------------------


Note : Ce programme est développé avec QtCreator, sous Linux. J'ai "fuit" Micro$oft depuis pas mal d'années maintenant, à la fois pour des raisons de manque de transparence, de manque de fiabilité , et de désaccord avec leur politique de gestion des données personnelles. 
myVideoDL pourrait donc à priori être compilé sous Windows dans la mesure ou Qt est multiplateforme, mais n'ayant pas testé, je vous laisse le plaisir d'en gérer les détails (librairies nécessaires, installations diverses, compilation, écrans bleus :-), etc...)

---------------------------------------------------------------------------------


Pour un fonctionnement correct, myVideoDL a besoin de  : 
 - python-pip
 - youtube-dl (sudo pip install --upgrade youtube_dl)
 - ffmpeg (pour le merge audio/video)
 - atomicparsley (gestion des metadatas)

---------------------------------------------------------------------------------


Version 2017.11.02 :

 - myVideoDL dispose de 4 slots de téléchargement, chacun gérant un process de youtube-dl indépendant des autres
 - myVideoDL gère les videos en provenance de : youtube (url contenant "youtu"), arte (url contenant "arte.tv") et franceTV (url contenant france.tv)
 - Les "radioButtons" permettent de choisir la qualité de la video à télécharger
    * HD1280 -> Video 1280x720 sur youtube - 1024x576 sur franceTV - Sur Arte, 720x406 si existe, sinon 1280x720
    * W854 -> Video 854x480 sur youtube - 704x396 sur franceTV - Sur Arte, 720x406 si existe, sinon 1280x720
    * W640 -> Video 640x360 sur youtube - 512x288 sur franceTV - Sur Arte, 720x406 si existe, sinon 1280x720
    * Audio Only -> mp4a@128k AAC sur youtube - à vérifier sur franceTV - mp4a@192K sur Arte
    * Video Only -> Video seule 1280x720 en webm sur youtube - 1024x576 sur franceTV - Sur Arte, 720x406 si existe, sinon 1280x720


Utilisation : 

a/ Utilisez le bouton "setPath" dans chaque slot pour définir le/les répertoire(s) vers lesquels vous souhaitez effectuer les téléchargements. Ces répertoires sont affichés dans la zone d'édition bleue.
Par défaut, les téléchargements s'effectueront dans votre répertoire utilisateur.
Ces répertoires seront mémorisés dans un fichier myVideoDL.ini, stocké dans votre répertoire utilisateur.

b/ A partir de votre navigateur, copiez l'url de la video à télécharger, puis collez-la dans la zone d'édition blanche du Slot à partir duquel vous souhaitez effectuer le téléchargement, puis cliquez ensuite sur le bouton "Télécharger".
Dans la zone d'édition orange, vous verrez défiler les messages d'état des téléchargement en temps réel.
Le bouton "Sources valides" en bas affiche simplement un tooltip indiquant les sites pris en charge.

---------------------------------------------------------------------------------



