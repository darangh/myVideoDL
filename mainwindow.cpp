#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDir>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    updateGuiTimer = new QTimer(this);
    updateGuiTimer->setSingleShot(true);
    connect (updateGuiTimer, SIGNAL(timeout()), this, SLOT(updateAppSize()));

    connect (&dlFormatProcess1, SIGNAL(readyRead()), this, SLOT(dlFormatProcess1ReadFromProcess()));
    connect (&dlFormatProcess1, SIGNAL(finished(int)), this, SLOT(dlFormatProcess1Finished(int)));
    connect (&dlFormatProcess2, SIGNAL(readyRead()), this, SLOT(dlFormatProcess2ReadFromProcess()));
    connect (&dlFormatProcess2, SIGNAL(finished(int)), this, SLOT(dlFormatProcess2Finished(int)));
    connect (&dlFormatProcess3, SIGNAL(readyRead()), this, SLOT(dlFormatProcess3ReadFromProcess()));
    connect (&dlFormatProcess3, SIGNAL(finished(int)), this, SLOT(dlFormatProcess3Finished(int)));
    connect (&dlFormatProcess4, SIGNAL(readyRead()), this, SLOT(dlFormatProcess4ReadFromProcess()));
    connect (&dlFormatProcess4, SIGNAL(finished(int)), this, SLOT(dlFormatProcess4Finished(int)));

    connect (&dlProcess1, SIGNAL(readyRead()), this, SLOT(dlProcess1ReadFromProcess()));
    connect (&dlProcess1, SIGNAL(finished(int)), this, SLOT(dlProcess1Finished(int)));
    connect (&dlProcess2, SIGNAL(readyRead()), this, SLOT(dlProcess2ReadFromProcess()));
    connect (&dlProcess2, SIGNAL(finished(int)), this, SLOT(dlProcess2Finished(int)));
    connect (&dlProcess3, SIGNAL(readyRead()), this, SLOT(dlProcess3ReadFromProcess()));
    connect (&dlProcess3, SIGNAL(finished(int)), this, SLOT(dlProcess3Finished(int)));
    connect (&dlProcess4, SIGNAL(readyRead()), this, SLOT(dlProcess4ReadFromProcess()));
    connect (&dlProcess4, SIGNAL(finished(int)), this, SLOT(dlProcess4Finished(int)));

    rwIniFile();
    updateGuiTimer->start(400);
}

MainWindow::~MainWindow()
{
    delete ui;
}


// ////// SLOT 1
//
//
void MainWindow::on_dl1Btn_clicked()
{    
    QString mydlProcess = "python /usr/local/bin/youtube-dl ";
    QString urlAddress = ui->dl1LE->text().trimmed();
    if (urlAddress.contains("youtu"))
    {
        if (ui->rbHD1280_1->isChecked()){mydlProcess.append("-f 22 -i --geo-bypass --write-description --write-thumbnail ");}//1280x720 mp4 avec mp4a@192 (HD720)
        if (ui->rbW854_1->isChecked())  {mydlProcess.append("-f 135+140 -i --geo-bypass --write-description --write-thumbnail ");}//854x480 mp4 et mp4a@128k
        if (ui->rbW640_1->isChecked())  {mydlProcess.append("-f 43 -i --geo-bypass --write-description --write-thumbnail ");}//640x360 webm avec vorbis@128k
        if (ui->rbVideoO_1->isChecked()){mydlProcess.append("-f 247 -i --geo-bypass --write-description --write-thumbnail ");}//Video 1280x720 en webm, sans audio -> Fonctionne beaucoup mieux que la version mp4 (-f 136), qui bouffe trop de ressources à la lecture !!!
        if (ui->rbAudioO_1->isChecked()){mydlProcess.append("-f 140 -i --geo-bypass --write-description --write-thumbnail --embed-thumbnail ");}//Format audio seul mp4a@128k AAC - Passer à 171 pour du vorbis@128k
    }
    if (urlAddress.contains("arte.tv"))
    {
        if (ui->rbAudioO_1->isChecked()){mydlProcess.append("-f HTTPS_HQ_1 -i -x --embed-thumbnail --geo-bypass --write-description --write-thumbnail ");}//Format audio seul mp4a@192K} // --audio-format \"best\" --audio-quality 128K
        else{mydlProcess.append("-f HTTPS_EQ_1/HTTPS_SQ_1/HLS_XQ_1/HTTPS_HQ_1/HTTPS_MQ_1 -i --geo-bypass --write-description --write-thumbnail ");}//mp4 720 x 406 de préférence
    }
    if (urlAddress.contains("france.tv"))
    {
        mydlProcess.append("-F ");//A ce stade, pour les videos de france.tv, on devra rapatrier la liste des formats disponibles
    }
    mydlProcess.append(urlAddress);
    if (urlAddress.contains("france.tv"))//Si c'est une adresse france.tv, on doit impérativement récupérer le nom du codec (il change pour chaque fichier !)
    {
        filenameFormatProc1 = lastPath1;
        filenameFormatProc1.append("/dl1FormatProcess.sh");
        QFile mydl1ProcessFile(filenameFormatProc1);
        if (mydl1ProcessFile.open(QIODevice::WriteOnly))
        {
            QTextStream outStream(&mydl1ProcessFile);
            outStream << "#!/bin/bash" << endl;
            outStream << "cd " << lastPath1 << endl;
            outStream << mydlProcess << endl;
            mydl1ProcessFile.close();
        }
        QString setCHMODProcess = "chmod +x ";
        setCHMODProcess.append(filenameFormatProc1);
        QProcess chmodProcess;
        chmodProcess.start(setCHMODProcess);
        chmodProcess.waitForFinished(2000);
        QString bashProcess = "bash -c ";
        bashProcess.append(filenameFormatProc1);
        ui->dl1Btn->setEnabled(false);
        ui->dl1LE->setReadOnly(true);
        ui->rbHD1280_1->setEnabled(false);
        ui->rbW854_1->setEnabled(false);
        ui->rbW640_1->setEnabled(false);
        ui->rbAudioO_1->setEnabled(false);
        ui->rbVideoO_1->setEnabled(false);
        dlFormatList1.clear();
        dlFormatProcess1.start(mydlProcess);
    }
    else // Si adresse youtube ou arte
    {
        if (ui->rbAudioO_1->isChecked()){mydlProcess.append(" -o \"" +lastPath1+ "/%(title)s - %(acodec)s%.(abr)dK.%(ext)s\"");}
        else {mydlProcess.append(" -o \"" +lastPath1+ "/%(title)s - %(resolution)s.%(ext)s\"");}
        filenameProc1 = lastPath1;
        filenameProc1.append("/dl1Process.sh");
        QFile mydl1ProcessFile(filenameProc1);
        if (mydl1ProcessFile.open(QIODevice::WriteOnly))
        {
            QTextStream outStream(&mydl1ProcessFile);
            outStream << "#!/bin/bash" << endl;
            outStream << "cd " << lastPath1 << endl;
            outStream << mydlProcess << endl;
            mydl1ProcessFile.close();
        }
        QString setCHMODProcess = "chmod +x ";
        setCHMODProcess.append(filenameProc1);
        QProcess chmodProcess;
        chmodProcess.start(setCHMODProcess);
        chmodProcess.waitForFinished(2000);
        QString bashProcess = "bash -c ";
        bashProcess.append(filenameProc1);
        ui->dl1Btn->setEnabled(false);
        ui->dl1LE->setReadOnly(true);
        ui->rbHD1280_1->setEnabled(false);
        ui->rbW854_1->setEnabled(false);
        ui->rbW640_1->setEnabled(false);
        ui->rbAudioO_1->setEnabled(false);
        ui->rbVideoO_1->setEnabled(false);
        dlProcess1.start(mydlProcess);
    }
}
void MainWindow::dlFormatProcess1ReadFromProcess()
{
    ui->fromdl1LE->setText(dlFormatProcess1.readAll());
    dlFormatList1.append(ui->fromdl1LE->text());
    ui->fromdl1LE->repaint();
}
void MainWindow::dlFormatProcess1Finished(int oneExitCode)
{
    if (oneExitCode==0)
    {
        if (dlFormatList1.count()!=0)
        {
            QString formatFound = "notFound";
            bool notFound = true;
            QString extractStr = dlFormatList1.at(dlFormatList1.count()-1);
            QStringList extractList = extractStr.split("\n");
            QStringList myArray; myArray.clear();
            for (int n=0; n<extractList.count(); n++)
            {
                if (notFound && (extractList.at(n).startsWith("hls_v") || extractList.at(n).startsWith("m3u8")))
                {
                    //Nb : ce process (dlFormatProcess) est utilisé pour france.tv - Les thumbnails ne sont pas gérés (et cela  peut générer une erreur, l'option--write-thumbnail a donc été enlevée de la ligne de commande)
                    QString myLine = extractList.at(n).simplified();
                    if ((ui->rbHD1280_1->isChecked() || ui->rbVideoO_1->isChecked()) && myLine.contains("1024x576"))//1024x576 mp4 - L'option Video seule n'existe pas, on téléchargera la version avec son ! (Comme en HD!)
                    {
                        notFound = false; myArray = myLine.split(" "); formatFound = "-f "; formatFound.append(myArray.at(0));
                        formatFound.append(" -i --geo-bypass --write-description "); //1024x512 mp4
                    }
                    if (ui->rbW854_1->isChecked() && myLine.contains("704x396"))
                    {
                        notFound = false; myArray = myLine.split(" "); formatFound = "-f "; formatFound.append(myArray.at(0));
                        formatFound.append(" -i --geo-bypass --write-description "); //704x396 mp4
                    }
                    if (ui->rbW640_1->isChecked() && myLine.contains("512x288"))
                    {
                        notFound = false; myArray = myLine.split(" "); formatFound = "-f "; formatFound.append(myArray.at(0));
                        formatFound.append(" -i --geo-bypass --write-description "); //512x288 mp4
                    }
                    if (ui->rbAudioO_1->isChecked() && myLine.contains("512x288"))
                    {
                        notFound = false; myArray = myLine.split(" "); formatFound = "-f "; formatFound.append(myArray.at(0));
                        formatFound.append(" -i --geo-bypass --write-description -x "); //512x288 mp4
                    }
                }
            }
            if (!notFound)//On a trouvé le format correspondant à la sélection RB
            {
                QFile mydl1FormatProcessFile(filenameFormatProc1);
                if (mydl1FormatProcessFile.exists()) mydl1FormatProcessFile.remove();//Pour effacer le fichier précédent

                QString mydlProcess = "python /usr/local/bin/youtube-dl ";
                QString urlAddress = ui->dl1LE->text().trimmed();
                mydlProcess.append(formatFound); mydlProcess.append(urlAddress);
                if (ui->rbAudioO_1->isChecked()){mydlProcess.append(" -o \"" +lastPath1+ "/%(title)s - %(acodec)s%.%(ext)s\"");}
                else {mydlProcess.append(" -o \"" +lastPath1+ "/%(title)s - %(resolution)s.%(ext)s\"");}
                filenameProc1 = lastPath1;
                filenameProc1.append("/dl1Process.sh");
                QFile mydl1ProcessFile(filenameProc1);
                if (mydl1ProcessFile.open(QIODevice::WriteOnly))
                {
                    QTextStream outStream(&mydl1ProcessFile);
                    outStream << "#!/bin/bash" << endl;
                    outStream << "cd "<< lastPath1 << endl;
                    outStream << mydlProcess << endl;
                    mydl1ProcessFile.close();
                }
                QString setCHMODProcess = "chmod +x ";
                setCHMODProcess.append(filenameProc1);
                QProcess chmodProcess;
                chmodProcess.start(setCHMODProcess);
                chmodProcess.waitForFinished(2000);
                QString bashProcess = "bash -c ";
                bashProcess.append(filenameProc1);
                dlProcess1.start(mydlProcess);
            }
        }
    }
    if (oneExitCode==1)
    {
        QString errorStr = dlFormatProcess1.readAllStandardError();
        if (errorStr.startsWith("ERROR: requested format not available"))
        {
            ui->fromdl1LE->setText(errorStr);ui->fromdl1LE->repaint();
            QMessageBox::information(this, "", "Le format demandé n'est pas disponible.\r\nEssayez un autre format.");
        }
        else
        {
            QMessageBox::information(this, "", "L'adresse du media en Slot 1 parait invalide.\r\nVeuillez entrer une adresse de media valide.");
        }
    }
    if (oneExitCode==2) QMessageBox::information(this, "", "Pas d'adresse de media en Slot 1.\r\nVeuillez entrer une adresse de media valide.");
    if (oneExitCode!=0)
    {
        ui->dl1Btn->setEnabled(true);
        ui->dl1LE->setReadOnly(false);
        ui->rbHD1280_1->setEnabled(true);
        ui->rbW854_1->setEnabled(true);
        ui->rbW640_1->setEnabled(true);
        ui->rbAudioO_1->setEnabled(true);
        ui->rbVideoO_1->setEnabled(true);
        QFile mydl1ProcessFile(filenameFormatProc1);
        if (mydl1ProcessFile.exists()) mydl1ProcessFile.remove();//Pour effacer le fichier précédent
    }
}
void MainWindow::dlProcess1ReadFromProcess()
{
    ui->fromdl1LE->setText(dlProcess1.readAll());
    ui->fromdl1LE->repaint();
}
void MainWindow::dlProcess1Finished(int oneExitCode)
{
    if (oneExitCode==0) ui->dl1LE->setText("Téléchargement sur Slot 1 terminé - Pas d'erreur");
    if (oneExitCode==1)
    {
        QString errorStr = dlProcess1.readAllStandardError();
        if (errorStr.startsWith("ERROR: requested format not available"))
        {
            ui->fromdl1LE->setText(errorStr);ui->fromdl1LE->repaint();
            QMessageBox::information(this, "", "Le format demandé n'est pas disponible.\r\nEssayez un autre format.");
        }
        else
        {
            QMessageBox::information(this, "", "L'adresse du media en Slot 1 parait invalide.\r\nVeuillez entrer une adresse de media valide.");
        }
    }
    if (oneExitCode==2) QMessageBox::information(this, "", "Pas d'adresse de media en Slot 1.\r\nVeuillez entrer une adresse de media valide.");
    ui->dl1Btn->setEnabled(true);
    ui->dl1LE->setReadOnly(false);
    ui->rbHD1280_1->setEnabled(true);
    ui->rbW854_1->setEnabled(true);
    ui->rbW640_1->setEnabled(true);
    ui->rbAudioO_1->setEnabled(true);
    ui->rbVideoO_1->setEnabled(true);
    QFile mydl1ProcessFile(filenameProc1);
    if (mydl1ProcessFile.exists()) mydl1ProcessFile.remove();//Pour effacer le fichier précédent
}

// ////// SLOT 2
//
//
void MainWindow::on_dl2Btn_clicked()
{
    QString mydlProcess = "python /usr/local/bin/youtube-dl ";
    QString urlAddress = ui->dl2LE->text().trimmed();
    if (urlAddress.contains("youtu"))
    {
        if (ui->rbHD1280_2->isChecked()){mydlProcess.append("-f 22 -i --geo-bypass --write-description --write-thumbnail ");}//1280x720 mp4 avec mp4a@192 (HD720)
        if (ui->rbW854_2->isChecked())  {mydlProcess.append("-f 135+140 -i --geo-bypass --write-description --write-thumbnail ");}//854x480 mp4 et mp4a@128k
        if (ui->rbW640_2->isChecked())  {mydlProcess.append("-f 43 -i --geo-bypass --write-description --write-thumbnail ");}//640x360 webm avec vorbis@128k
        if (ui->rbVideoO_2->isChecked()){mydlProcess.append("-f 247 -i --geo-bypass --write-description --write-thumbnail ");}//Video 1280x720 en webm, sans audio -> Fonctionne beaucoup mieux que la version mp4 (-f 136), qui bouffe trop de ressources à la lecture !!!
        if (ui->rbAudioO_2->isChecked()){mydlProcess.append("-f 140 -i --geo-bypass --write-description --write-thumbnail --embed-thumbnail ");}//Format audio seul mp4a@128k AAC - Passer à 171 pour du vorbis@128k
    }
    if (urlAddress.contains("arte.tv"))
    {
        if (ui->rbAudioO_2->isChecked()){mydlProcess.append("-f HTTPS_HQ_1 -i -x --embed-thumbnail --geo-bypass --write-description --write-thumbnail ");}//Format audio seul mp4a@192K} // --audio-format \"best\" --audio-quality 128K
        else{mydlProcess.append("-f HTTPS_EQ_1/HTTPS_SQ_1/HLS_XQ_1/HTTPS_HQ_1/HTTPS_MQ_1 -i --geo-bypass --write-description --write-thumbnail ");}//mp4 720 x 406 de préférence
    }
    if (urlAddress.contains("france.tv"))
    {
        mydlProcess.append("-F ");//A ce stade, pour les videos de france.tv, on devra rapatrier la liste des formats disponibles
    }
    mydlProcess.append(urlAddress);
    if (urlAddress.contains("france.tv"))//Si c'est une adresse france.tv, on doit impérativement récupérer le nom du codec (il change pour chaque fichier !)
    {
        filenameFormatProc2 = lastPath2;
        filenameFormatProc2.append("/dl2FormatProcess.sh");
        QFile mydl2ProcessFile(filenameFormatProc2);
        if (mydl2ProcessFile.open(QIODevice::WriteOnly))
        {
            QTextStream outStream(&mydl2ProcessFile);
            outStream << "#!/bin/bash" << endl;
            outStream << "cd " << lastPath2 << endl;
            outStream << mydlProcess << endl;
            mydl2ProcessFile.close();
        }
        QString setCHMODProcess = "chmod +x ";
        setCHMODProcess.append(filenameFormatProc2);
        QProcess chmodProcess;
        chmodProcess.start(setCHMODProcess);
        chmodProcess.waitForFinished(2000);
        QString bashProcess = "bash -c ";
        bashProcess.append(filenameFormatProc2);
        ui->dl2Btn->setEnabled(false);
        ui->dl2LE->setReadOnly(true);
        ui->rbHD1280_2->setEnabled(false);
        ui->rbW854_2->setEnabled(false);
        ui->rbW640_2->setEnabled(false);
        ui->rbAudioO_2->setEnabled(false);
        ui->rbVideoO_2->setEnabled(false);
        dlFormatList2.clear();
        dlFormatProcess2.start(mydlProcess);
    }
    else // Si adresse youtube ou arte
    {
        if (ui->rbAudioO_2->isChecked()){mydlProcess.append(" -o \"" +lastPath2+ "/%(title)s - %(acodec)s%.(abr)dK.%(ext)s\"");}
        else {mydlProcess.append(" -o \"" +lastPath2+ "/%(title)s - %(resolution)s.%(ext)s\"");}
        filenameProc2 = lastPath2;
        filenameProc2.append("/dl2Process.sh");
        QFile mydl2ProcessFile(filenameProc2);
        if (mydl2ProcessFile.open(QIODevice::WriteOnly))
        {
            QTextStream outStream(&mydl2ProcessFile);
            outStream << "#!/bin/bash" << endl;
            outStream << "cd " << lastPath2 << endl;
            outStream << mydlProcess << endl;
            mydl2ProcessFile.close();
        }
        QString setCHMODProcess = "chmod +x ";
        setCHMODProcess.append(filenameProc2);
        QProcess chmodProcess;
        chmodProcess.start(setCHMODProcess);
        chmodProcess.waitForFinished(2000);
        QString bashProcess = "bash -c ";
        bashProcess.append(filenameProc2);
        ui->dl2Btn->setEnabled(false);
        ui->dl2LE->setReadOnly(true);
        ui->rbHD1280_2->setEnabled(false);
        ui->rbW854_2->setEnabled(false);
        ui->rbW640_2->setEnabled(false);
        ui->rbAudioO_2->setEnabled(false);
        ui->rbVideoO_2->setEnabled(false);
        dlProcess2.start(mydlProcess);
    }
}
void MainWindow::dlFormatProcess2ReadFromProcess()
{
    ui->fromdl2LE->setText(dlFormatProcess2.readAll());
    dlFormatList2.append(ui->fromdl2LE->text());
    ui->fromdl2LE->repaint();
}
void MainWindow::dlFormatProcess2Finished(int oneExitCode)
{
    if (oneExitCode==0)
    {
        if (dlFormatList2.count()!=0)
        {
            QString formatFound = "notFound";
            bool notFound = true;
            QString extractStr = dlFormatList2.at(dlFormatList2.count()-1);
            QStringList extractList = extractStr.split("\n");
            QStringList myArray; myArray.clear();
            for (int n=0; n<extractList.count(); n++)
            {
                if (notFound && (extractList.at(n).startsWith("hls_v") || extractList.at(n).startsWith("m3u8")))
                {
                    //Nb : ce process (dlFormatProcess) est utilisé pour france.tv - Les thumbnails ne sont pas gérés (et cela  peut générer une erreur, l'option--write-thumbnail a donc été enlevée de la ligne de commande)
                    QString myLine = extractList.at(n).simplified();
                    if ((ui->rbHD1280_2->isChecked() || ui->rbVideoO_2->isChecked()) && myLine.contains("1024x576"))//1024x576 mp4 - L'option Video seule n'existe pas, on téléchargera la version avec son ! (Comme en HD!)
                    {
                        notFound = false; myArray = myLine.split(" "); formatFound = "-f "; formatFound.append(myArray.at(0));
                        formatFound.append(" -i --geo-bypass --write-description "); //1024x512 mp4
                    }
                    if (ui->rbW854_2->isChecked() && myLine.contains("704x396"))
                    {
                        notFound = false; myArray = myLine.split(" "); formatFound = "-f "; formatFound.append(myArray.at(0));
                        formatFound.append(" -i --geo-bypass --write-description "); //704x396 mp4
                    }
                    if (ui->rbW640_2->isChecked() && myLine.contains("512x288"))
                    {
                        notFound = false; myArray = myLine.split(" "); formatFound = "-f "; formatFound.append(myArray.at(0));
                        formatFound.append(" -i --geo-bypass --write-description "); //512x288 mp4
                    }
                    if (ui->rbAudioO_2->isChecked() && myLine.contains("512x288"))
                    {
                        notFound = false; myArray = myLine.split(" "); formatFound = "-f "; formatFound.append(myArray.at(0));
                        formatFound.append(" -i --geo-bypass --write-description -x "); //512x288 mp4
                    }
                }
            }
            if (!notFound)//On a trouvé le format correspondant à la sélection RB
            {
                QFile mydl2FormatProcessFile(filenameFormatProc2);
                if (mydl2FormatProcessFile.exists()) mydl2FormatProcessFile.remove();//Pour effacer le fichier précédent

                QString mydlProcess = "python /usr/local/bin/youtube-dl ";
                QString urlAddress = ui->dl2LE->text().trimmed();
                mydlProcess.append(formatFound); mydlProcess.append(urlAddress);
                if (ui->rbAudioO_2->isChecked()){mydlProcess.append(" -o \"" +lastPath2+ "/%(title)s - %(acodec)s%.%(ext)s\"");}
                else {mydlProcess.append(" -o \"" +lastPath2+ "/%(title)s - %(resolution)s.%(ext)s\"");}
                filenameProc2 = lastPath2;
                filenameProc2.append("/dl2Process.sh");
                QFile mydl2ProcessFile(filenameProc2);
                if (mydl2ProcessFile.open(QIODevice::WriteOnly))
                {
                    QTextStream outStream(&mydl2ProcessFile);
                    outStream << "#!/bin/bash" << endl;
                    outStream << "cd " << lastPath2 << endl;
                    outStream << mydlProcess << endl;
                    mydl2ProcessFile.close();
                }
                QString setCHMODProcess = "chmod +x ";
                setCHMODProcess.append(filenameProc2);
                QProcess chmodProcess;
                chmodProcess.start(setCHMODProcess);
                chmodProcess.waitForFinished(2000);
                QString bashProcess = "bash -c ";
                bashProcess.append(filenameProc2);
                dlProcess2.start(mydlProcess);
            }
        }
    }
    if (oneExitCode==1)
    {
        QString errorStr = dlFormatProcess2.readAllStandardError();
        if (errorStr.startsWith("ERROR: requested format not available"))
        {
            ui->fromdl2LE->setText(errorStr);ui->fromdl2LE->repaint();
            QMessageBox::information(this, "", "Le format demandé n'est pas disponible.\r\nEssayez un autre format.");
        }
        else
        {
            QMessageBox::information(this, "", "L'adresse du media en Slot 2 parait invalide.\r\nVeuillez entrer une adresse de media valide.");
        }
    }
    if (oneExitCode==2) QMessageBox::information(this, "", "Pas d'adresse de media en Slot 2.\r\nVeuillez entrer une adresse de media valide.");
    if (oneExitCode!=0)
    {
        ui->dl2Btn->setEnabled(true);
        ui->dl2LE->setReadOnly(false);
        ui->rbHD1280_2->setEnabled(true);
        ui->rbW854_2->setEnabled(true);
        ui->rbW640_2->setEnabled(true);
        ui->rbAudioO_2->setEnabled(true);
        ui->rbVideoO_2->setEnabled(true);
        QFile mydl2ProcessFile(filenameFormatProc2);
        if (mydl2ProcessFile.exists()) mydl2ProcessFile.remove();//Pour effacer le fichier précédent
    }
}
void MainWindow::dlProcess2ReadFromProcess()
{
    ui->fromdl2LE->setText(dlProcess2.readAll());
    ui->fromdl2LE->repaint();
}
void MainWindow::dlProcess2Finished(int oneExitCode)
{
    if (oneExitCode==0) ui->dl2LE->setText("Téléchargement sur Slot 2 terminé - Pas d'erreur");
    if (oneExitCode==1)
    {
        QString errorStr = dlProcess2.readAllStandardError();
        if (errorStr.startsWith("ERROR: requested format not available"))
        {
            ui->fromdl2LE->setText(errorStr);ui->fromdl2LE->repaint();
            QMessageBox::information(this, "", "Le format demandé n'est pas disponible.\r\nEssayez un autre format.");
        }
        else
        {
            QMessageBox::information(this, "", "L'adresse du media en Slot 2 parait invalide.\r\nVeuillez entrer une adresse de media valide.");
        }
    }
    if (oneExitCode==2) QMessageBox::information(this, "", "Pas d'adresse de media en Slot 2.\r\nVeuillez entrer une adresse de media valide.");
    ui->dl2Btn->setEnabled(true);
    ui->dl2LE->setReadOnly(false);
    ui->rbHD1280_2->setEnabled(true);
    ui->rbW854_2->setEnabled(true);
    ui->rbW640_2->setEnabled(true);
    ui->rbAudioO_2->setEnabled(true);
    ui->rbVideoO_2->setEnabled(true);
    QFile mydl2ProcessFile(filenameProc2);
    if (mydl2ProcessFile.exists()) mydl2ProcessFile.remove();//Pour effacer le fichier précédent
}

// ////// SLOT 3
//
//
void MainWindow::on_dl3Btn_clicked()
{
    QString mydlProcess = "python /usr/local/bin/youtube-dl ";
    QString urlAddress = ui->dl3LE->text().trimmed();
    if (urlAddress.contains("youtu"))
    {
        if (ui->rbHD1280_3->isChecked()){mydlProcess.append("-f 22 -i --geo-bypass --write-description --write-thumbnail ");}//1280x720 mp4 avec mp4a@192 (HD720)
        if (ui->rbW854_3->isChecked())  {mydlProcess.append("-f 135+140 -i --geo-bypass --write-description --write-thumbnail ");}//854x480 mp4 et mp4a@128k
        if (ui->rbW640_3->isChecked())  {mydlProcess.append("-f 43 -i --geo-bypass --write-description --write-thumbnail ");}//640x360 webm avec vorbis@128k
        if (ui->rbVideoO_3->isChecked()){mydlProcess.append("-f 247 -i --geo-bypass --write-description --write-thumbnail ");}//Video 1280x720 en webm, sans audio -> Fonctionne beaucoup mieux que la version mp4 (-f 136), qui bouffe trop de ressources à la lecture !!!
        if (ui->rbAudioO_3->isChecked()){mydlProcess.append("-f 140 -i --geo-bypass --write-description --write-thumbnail --embed-thumbnail ");}//Format audio seul mp4a@128k AAC - Passer à 171 pour du vorbis@128k
    }
    if (urlAddress.contains("arte.tv"))
    {
        if (ui->rbAudioO_3->isChecked()){mydlProcess.append("-f HTTPS_HQ_1 -i -x --embed-thumbnail --geo-bypass --write-description --write-thumbnail ");}//Format audio seul mp4a@192K} // --audio-format \"best\" --audio-quality 128K
        else{mydlProcess.append("-f HTTPS_EQ_1/HTTPS_SQ_1/HLS_XQ_1/HTTPS_HQ_1/HTTPS_MQ_1 -i --geo-bypass --write-description --write-thumbnail ");}//mp4 720 x 406 de préférence
    }
    if (urlAddress.contains("france.tv"))
    {
        mydlProcess.append("-F ");//A ce stade, pour les videos de france.tv, on devra rapatrier la liste des formats disponibles
    }
    mydlProcess.append(urlAddress);
    if (urlAddress.contains("france.tv"))//Si c'est une adresse france.tv, on doit impérativement récupérer le nom du codec (il change pour chaque fichier !)
    {
        filenameFormatProc3 = lastPath3;
        filenameFormatProc3.append("/dl3FormatProcess.sh");
        QFile mydl3ProcessFile(filenameFormatProc3);
        if (mydl3ProcessFile.open(QIODevice::WriteOnly))
        {
            QTextStream outStream(&mydl3ProcessFile);
            outStream << "#!/bin/bash" << endl;
            outStream << "cd " << lastPath3 << endl;
            outStream << mydlProcess << endl;
            mydl3ProcessFile.close();
        }
        QString setCHMODProcess = "chmod +x ";
        setCHMODProcess.append(filenameFormatProc3);
        QProcess chmodProcess;
        chmodProcess.start(setCHMODProcess);
        chmodProcess.waitForFinished(2000);
        QString bashProcess = "bash -c ";
        bashProcess.append(filenameFormatProc3);
        ui->dl3Btn->setEnabled(false);
        ui->dl3LE->setReadOnly(true);
        ui->rbHD1280_3->setEnabled(false);
        ui->rbW854_3->setEnabled(false);
        ui->rbW640_3->setEnabled(false);
        ui->rbAudioO_3->setEnabled(false);
        ui->rbVideoO_3->setEnabled(false);
        dlFormatList3.clear();
        dlFormatProcess3.start(mydlProcess);
    }
    else // Si adresse youtube ou arte
    {
        if (ui->rbAudioO_3->isChecked()){mydlProcess.append(" -o \"" +lastPath3+ "/%(title)s - %(acodec)s%.(abr)dK.%(ext)s\"");}
        else {mydlProcess.append(" -o \"" +lastPath3+ "/%(title)s - %(resolution)s.%(ext)s\"");}
        filenameProc3 = lastPath3;
        filenameProc3.append("/dl3Process.sh");
        QFile mydl3ProcessFile(filenameProc3);
        if (mydl3ProcessFile.open(QIODevice::WriteOnly))
        {
            QTextStream outStream(&mydl3ProcessFile);
            outStream << "#!/bin/bash" << endl;
            outStream << "cd " << lastPath3 << endl;
            outStream << mydlProcess << endl;
            mydl3ProcessFile.close();
        }
        QString setCHMODProcess = "chmod +x ";
        setCHMODProcess.append(filenameProc3);
        QProcess chmodProcess;
        chmodProcess.start(setCHMODProcess);
        chmodProcess.waitForFinished(2000);
        QString bashProcess = "bash -c ";
        bashProcess.append(filenameProc3);
        ui->dl3Btn->setEnabled(false);
        ui->dl3LE->setReadOnly(true);
        ui->rbHD1280_3->setEnabled(false);
        ui->rbW854_3->setEnabled(false);
        ui->rbW640_3->setEnabled(false);
        ui->rbAudioO_3->setEnabled(false);
        ui->rbVideoO_3->setEnabled(false);
        dlProcess3.start(mydlProcess);
    }
}
void MainWindow::dlFormatProcess3ReadFromProcess()
{
    ui->fromdl3LE->setText(dlFormatProcess3.readAll());
    dlFormatList3.append(ui->fromdl3LE->text());
    ui->fromdl3LE->repaint();
}
void MainWindow::dlFormatProcess3Finished(int oneExitCode)
{
    if (oneExitCode==0)
    {
        if (dlFormatList3.count()!=0)
        {
            QString formatFound = "notFound";
            bool notFound = true;
            QString extractStr = dlFormatList3.at(dlFormatList3.count()-1);
            QStringList extractList = extractStr.split("\n");
            QStringList myArray; myArray.clear();
            for (int n=0; n<extractList.count(); n++)
            {
                if (notFound && (extractList.at(n).startsWith("hls_v") || extractList.at(n).startsWith("m3u8")))
                {
                    //Nb : ce process (dlFormatProcess) est utilisé pour france.tv - Les thumbnails ne sont pas gérés (et cela  peut générer une erreur, l'option--write-thumbnail a donc été enlevée de la ligne de commande)
                    QString myLine = extractList.at(n).simplified();
                    if ((ui->rbHD1280_3->isChecked() || ui->rbVideoO_2->isChecked()) && myLine.contains("1024x576"))//1024x576 mp4 - L'option Video seule n'existe pas, on téléchargera la version avec son ! (Comme en HD!)
                    {
                        notFound = false; myArray = myLine.split(" "); formatFound = "-f "; formatFound.append(myArray.at(0));
                        formatFound.append(" -i --geo-bypass --write-description "); //1024x512 mp4
                    }
                    if (ui->rbW854_3->isChecked() && myLine.contains("704x396"))
                    {
                        notFound = false; myArray = myLine.split(" "); formatFound = "-f "; formatFound.append(myArray.at(0));
                        formatFound.append(" -i --geo-bypass --write-description "); //704x396 mp4
                    }
                    if (ui->rbW640_3->isChecked() && myLine.contains("512x288"))
                    {
                        notFound = false; myArray = myLine.split(" "); formatFound = "-f "; formatFound.append(myArray.at(0));
                        formatFound.append(" -i --geo-bypass --write-description "); //512x288 mp4
                    }
                    if (ui->rbAudioO_3->isChecked() && myLine.contains("512x288"))
                    {
                        notFound = false; myArray = myLine.split(" "); formatFound = "-f "; formatFound.append(myArray.at(0));
                        formatFound.append(" -i --geo-bypass --write-description -x "); //512x288 mp4
                    }
                }
            }
            if (!notFound)//On a trouvé le format correspondant à la sélection RB
            {
                QFile mydl3FormatProcessFile(filenameFormatProc3);
                if (mydl3FormatProcessFile.exists()) mydl3FormatProcessFile.remove();//Pour effacer le fichier précédent

                QString mydlProcess = "python /usr/local/bin/youtube-dl ";
                QString urlAddress = ui->dl3LE->text().trimmed();
                mydlProcess.append(formatFound); mydlProcess.append(urlAddress);
                if (ui->rbAudioO_3->isChecked()){mydlProcess.append(" -o \"" +lastPath3+ "/%(title)s - %(acodec)s%.%(ext)s\"");}
                else {mydlProcess.append(" -o \"" +lastPath3+ "/%(title)s - %(resolution)s.%(ext)s\"");}
                filenameProc3 = lastPath3;
                filenameProc3.append("/dl3Process.sh");
                QFile mydl3ProcessFile(filenameProc3);
                if (mydl3ProcessFile.open(QIODevice::WriteOnly))
                {
                    QTextStream outStream(&mydl3ProcessFile);
                    outStream << "#!/bin/bash" << endl;
                    outStream << "cd " << lastPath3 << endl;
                    outStream << mydlProcess << endl;
                    mydl3ProcessFile.close();
                }
                QString setCHMODProcess = "chmod +x ";
                setCHMODProcess.append(filenameProc3);
                QProcess chmodProcess;
                chmodProcess.start(setCHMODProcess);
                chmodProcess.waitForFinished(2000);
                QString bashProcess = "bash -c ";
                bashProcess.append(filenameProc3);
                dlProcess3.start(mydlProcess);
            }
        }
    }
    if (oneExitCode==1)
    {
        QString errorStr = dlFormatProcess3.readAllStandardError();
        if (errorStr.startsWith("ERROR: requested format not available"))
        {
            ui->fromdl3LE->setText(errorStr);ui->fromdl3LE->repaint();
            QMessageBox::information(this, "", "Le format demandé n'est pas disponible.\r\nEssayez un autre format.");
        }
        else
        {
            QMessageBox::information(this, "", "L'adresse du media en Slot 3 parait invalide.\r\nVeuillez entrer une adresse de media valide.");
        }
    }
    if (oneExitCode==2) QMessageBox::information(this, "", "Pas d'adresse de media en Slot 3.\r\nVeuillez entrer une adresse de media valide.");
    if (oneExitCode!=0)
    {
        ui->dl3Btn->setEnabled(true);
        ui->dl3LE->setReadOnly(false);
        ui->rbHD1280_3->setEnabled(true);
        ui->rbW854_3->setEnabled(true);
        ui->rbW640_3->setEnabled(true);
        ui->rbAudioO_3->setEnabled(true);
        ui->rbVideoO_3->setEnabled(true);
        QFile mydl3ProcessFile(filenameFormatProc3);
        if (mydl3ProcessFile.exists()) mydl3ProcessFile.remove();//Pour effacer le fichier précédent
    }
}
void MainWindow::dlProcess3ReadFromProcess()
{
    ui->fromdl3LE->setText(dlProcess3.readAll());
    ui->fromdl3LE->repaint();
}
void MainWindow::dlProcess3Finished(int oneExitCode)
{
    if (oneExitCode==0) ui->dl3LE->setText("Téléchargement sur Slot 3 terminé - Pas d'erreur");
    if (oneExitCode==1)
    {
        QString errorStr = dlProcess3.readAllStandardError();
        if (errorStr.startsWith("ERROR: requested format not available"))
        {
            ui->fromdl3LE->setText(errorStr);ui->fromdl3LE->repaint();
            QMessageBox::information(this, "", "Le format demandé n'est pas disponible.\r\nEssayez un autre format.");
        }
        else
        {
            QMessageBox::information(this, "", "L'adresse du media en Slot 3 parait invalide.\r\nVeuillez entrer une adresse de media valide.");
        }
    }
    if (oneExitCode==2) QMessageBox::information(this, "", "Pas d'adresse de media en Slot 3.\r\nVeuillez entrer une adresse de media valide.");
    ui->dl3Btn->setEnabled(true);
    ui->dl3LE->setReadOnly(false);
    ui->rbHD1280_3->setEnabled(true);
    ui->rbW854_3->setEnabled(true);
    ui->rbW640_3->setEnabled(true);
    ui->rbAudioO_3->setEnabled(true);
    ui->rbVideoO_3->setEnabled(true);
    QFile mydl3ProcessFile(filenameProc3);
    if (mydl3ProcessFile.exists()) mydl3ProcessFile.remove();//Pour effacer le fichier précédent
}

// ////// SLOT 4
//
//
void MainWindow::on_dl4Btn_clicked()
{
    QString mydlProcess = "python /usr/local/bin/youtube-dl ";
    QString urlAddress = ui->dl4LE->text().trimmed();
    if (urlAddress.contains("youtu"))
    {
        if (ui->rbHD1280_4->isChecked()){mydlProcess.append("-f 22 -i --geo-bypass --write-description --write-thumbnail ");}//1280x720 mp4 avec mp4a@192 (HD720)
        if (ui->rbW854_4->isChecked())  {mydlProcess.append("-f 135+140 -i --geo-bypass --write-description --write-thumbnail ");}//854x480 mp4 et mp4a@128k
        if (ui->rbW640_4->isChecked())  {mydlProcess.append("-f 43 -i --geo-bypass --write-description --write-thumbnail ");}//640x360 webm avec vorbis@128k
        if (ui->rbVideoO_4->isChecked()){mydlProcess.append("-f 247 -i --geo-bypass --write-description --write-thumbnail ");}//Video 1280x720 en webm, sans audio -> Fonctionne beaucoup mieux que la version mp4 (-f 136), qui bouffe trop de ressources à la lecture !!!
        if (ui->rbAudioO_4->isChecked()){mydlProcess.append("-f 140 -i --geo-bypass --write-description --write-thumbnail --embed-thumbnail ");}//Format audio seul mp4a@128k AAC - Passer à 171 pour du vorbis@128k
    }
    if (urlAddress.contains("arte.tv"))
    {
        if (ui->rbAudioO_4->isChecked()){mydlProcess.append("-f HTTPS_HQ_1 -i -x --embed-thumbnail --geo-bypass --write-description --write-thumbnail ");}//Format audio seul mp4a@192K} // --audio-format \"best\" --audio-quality 128K
        else{mydlProcess.append("-f HTTPS_EQ_1/HTTPS_SQ_1/HLS_XQ_1/HTTPS_HQ_1/HTTPS_MQ_1 -i --geo-bypass --write-description --write-thumbnail ");}//mp4 720 x 406 de préférence
    }
    if (urlAddress.contains("france.tv"))
    {
        mydlProcess.append("-F ");//A ce stade, pour les videos de france.tv, on devra rapatrier la liste des formats disponibles
    }
    mydlProcess.append(urlAddress);
    if (urlAddress.contains("france.tv"))//Si c'est une adresse france.tv, on doit impérativement récupérer le nom du codec (il change pour chaque fichier !)
    {
        filenameFormatProc4 = lastPath4;
        filenameFormatProc4.append("/dl4FormatProcess.sh");
        QFile mydl4ProcessFile(filenameFormatProc4);
        if (mydl4ProcessFile.open(QIODevice::WriteOnly))
        {
            QTextStream outStream(&mydl4ProcessFile);
            outStream << "#!/bin/bash" << endl;
            outStream << "cd " << lastPath4 << endl;
            outStream << mydlProcess << endl;
            mydl4ProcessFile.close();
        }
        QString setCHMODProcess = "chmod +x ";
        setCHMODProcess.append(filenameFormatProc4);
        QProcess chmodProcess;
        chmodProcess.start(setCHMODProcess);
        chmodProcess.waitForFinished(2000);
        QString bashProcess = "bash -c ";
        bashProcess.append(filenameFormatProc4);
        ui->dl4Btn->setEnabled(false);
        ui->dl4LE->setReadOnly(true);
        ui->rbHD1280_4->setEnabled(false);
        ui->rbW854_4->setEnabled(false);
        ui->rbW640_4->setEnabled(false);
        ui->rbAudioO_4->setEnabled(false);
        ui->rbVideoO_4->setEnabled(false);
        dlFormatList4.clear();
        dlFormatProcess4.start(mydlProcess);
    }
    else // Si adresse youtube ou arte
    {
        if (ui->rbAudioO_4->isChecked()){mydlProcess.append(" -o \"" +lastPath4+ "/%(title)s - %(acodec)s%.(abr)dK.%(ext)s\"");}
        else {mydlProcess.append(" -o \"" +lastPath4+ "/%(title)s - %(resolution)s.%(ext)s\"");}
        filenameProc4 = lastPath4;
        filenameProc4.append("/dl4Process.sh");
        QFile mydl4ProcessFile(filenameProc4);
        if (mydl4ProcessFile.open(QIODevice::WriteOnly))
        {
            QTextStream outStream(&mydl4ProcessFile);
            outStream << "#!/bin/bash" << endl;
            outStream << "cd " << lastPath4 << endl;
            outStream << mydlProcess << endl;
            mydl4ProcessFile.close();
        }
        QString setCHMODProcess = "chmod +x ";
        setCHMODProcess.append(filenameProc4);
        QProcess chmodProcess;
        chmodProcess.start(setCHMODProcess);
        chmodProcess.waitForFinished(2000);
        QString bashProcess = "bash -c ";
        bashProcess.append(filenameProc4);
        ui->dl4Btn->setEnabled(false);
        ui->dl4LE->setReadOnly(true);
        ui->rbHD1280_4->setEnabled(false);
        ui->rbW854_4->setEnabled(false);
        ui->rbW640_4->setEnabled(false);
        ui->rbAudioO_4->setEnabled(false);
        ui->rbVideoO_4->setEnabled(false);
        dlProcess4.start(mydlProcess);
    }
}
void MainWindow::dlFormatProcess4ReadFromProcess()
{
    ui->fromdl4LE->setText(dlFormatProcess4.readAll());
    dlFormatList4.append(ui->fromdl4LE->text());
    ui->fromdl4LE->repaint();
}
void MainWindow::dlFormatProcess4Finished(int oneExitCode)
{
    if (oneExitCode==0)
    {
        if (dlFormatList4.count()!=0)
        {
            QString formatFound = "notFound";
            bool notFound = true;
            QString extractStr = dlFormatList4.at(dlFormatList4.count()-1);
            QStringList extractList = extractStr.split("\n");
            QStringList myArray; myArray.clear();
            for (int n=0; n<extractList.count(); n++)
            {
                if (notFound && (extractList.at(n).startsWith("hls_v") || extractList.at(n).startsWith("m3u8")))
                {
                    //Nb : ce process (dlFormatProcess) est utilisé pour france.tv - Les thumbnails ne sont pas gérés (et cela  peut générer une erreur, l'option--write-thumbnail a donc été enlevée de la ligne de commande)
                    QString myLine = extractList.at(n).simplified();
                    if ((ui->rbHD1280_4->isChecked() || ui->rbVideoO_4->isChecked()) && myLine.contains("1024x576"))//1024x576 mp4 - L'option Video seule n'existe pas, on téléchargera la version avec son ! (Comme en HD!)
                    {
                        notFound = false; myArray = myLine.split(" "); formatFound = "-f "; formatFound.append(myArray.at(0));
                        formatFound.append(" -i --geo-bypass --write-description "); //1024x512 mp4
                    }
                    if (ui->rbW854_4->isChecked() && myLine.contains("704x396"))
                    {
                        notFound = false; myArray = myLine.split(" "); formatFound = "-f "; formatFound.append(myArray.at(0));
                        formatFound.append(" -i --geo-bypass --write-description "); //704x396 mp4
                    }
                    if (ui->rbW640_4->isChecked() && myLine.contains("512x288"))
                    {
                        notFound = false; myArray = myLine.split(" "); formatFound = "-f "; formatFound.append(myArray.at(0));
                        formatFound.append(" -i --geo-bypass --write-description "); //512x288 mp4
                    }
                    if (ui->rbAudioO_4->isChecked() && myLine.contains("512x288"))
                    {
                        notFound = false; myArray = myLine.split(" "); formatFound = "-f "; formatFound.append(myArray.at(0));
                        formatFound.append(" -i --geo-bypass --write-description -x "); //512x288 mp4
                    }
                }
            }
            if (!notFound)//On a trouvé le format correspondant à la sélection RB
            {
                QFile mydl4FormatProcessFile(filenameFormatProc4);
                if (mydl4FormatProcessFile.exists()) mydl4FormatProcessFile.remove();//Pour effacer le fichier précédent

                QString mydlProcess = "python /usr/local/bin/youtube-dl ";
                QString urlAddress = ui->dl4LE->text().trimmed();
                mydlProcess.append(formatFound); mydlProcess.append(urlAddress);
                if (ui->rbAudioO_4->isChecked()){mydlProcess.append(" -o \"" +lastPath4+ "/%(title)s - %(acodec)s%.%(ext)s\"");}
                else {mydlProcess.append(" -o \"" +lastPath4+ "/%(title)s - %(resolution)s.%(ext)s\"");}
                filenameProc4 = lastPath4;
                filenameProc4.append("/dl4Process.sh");
                QFile mydl4ProcessFile(filenameProc4);
                if (mydl4ProcessFile.open(QIODevice::WriteOnly))
                {
                    QTextStream outStream(&mydl4ProcessFile);
                    outStream << "#!/bin/bash" << endl;
                    outStream << "cd " << lastPath4 << endl;
                    outStream << mydlProcess << endl;
                    mydl4ProcessFile.close();
                }
                QString setCHMODProcess = "chmod +x ";
                setCHMODProcess.append(filenameProc4);
                QProcess chmodProcess;
                chmodProcess.start(setCHMODProcess);
                chmodProcess.waitForFinished(2000);
                QString bashProcess = "bash -c ";
                bashProcess.append(filenameProc4);
                dlProcess4.start(mydlProcess);
            }
        }
    }
    if (oneExitCode==1)
    {
        QString errorStr = dlFormatProcess4.readAllStandardError();
        if (errorStr.startsWith("ERROR: requested format not available"))
        {
            ui->fromdl4LE->setText(errorStr);ui->fromdl4LE->repaint();
            QMessageBox::information(this, "", "Le format demandé n'est pas disponible.\r\nEssayez un autre format.");
        }
        else
        {
            QMessageBox::information(this, "", "L'adresse du media en Slot 4 parait invalide.\r\nVeuillez entrer une adresse de media valide.");
        }
    }
    if (oneExitCode==2) QMessageBox::information(this, "", "Pas d'adresse de media en Slot 4.\r\nVeuillez entrer une adresse de media valide.");
    if (oneExitCode!=0)
    {
        ui->dl4Btn->setEnabled(true);
        ui->dl4LE->setReadOnly(false);
        ui->rbHD1280_4->setEnabled(true);
        ui->rbW854_4->setEnabled(true);
        ui->rbW640_4->setEnabled(true);
        ui->rbAudioO_4->setEnabled(true);
        ui->rbVideoO_4->setEnabled(true);
        QFile mydl4ProcessFile(filenameFormatProc4);
        if (mydl4ProcessFile.exists()) mydl4ProcessFile.remove();//Pour effacer le fichier précédent
    }
}
void MainWindow::dlProcess4ReadFromProcess()
{
    ui->fromdl4LE->setText(dlProcess4.readAll());
    ui->fromdl4LE->repaint();
}
void MainWindow::dlProcess4Finished(int oneExitCode)
{
    if (oneExitCode==0) ui->dl4LE->setText("Téléchargement sur Slot 4 terminé - Pas d'erreur");
    if (oneExitCode==1)
    {
        QString errorStr = dlProcess4.readAllStandardError();
        if (errorStr.startsWith("ERROR: requested format not available"))
        {
            ui->fromdl4LE->setText(errorStr);ui->fromdl4LE->repaint();
            QMessageBox::information(this, "", "Le format demandé n'est pas disponible.\r\nEssayez un autre format.");
        }
        else
        {
            QMessageBox::information(this, "", "L'adresse du media en Slot 4 parait invalide.\r\nVeuillez entrer une adresse de media valide.");
        }
    }
    if (oneExitCode==2) QMessageBox::information(this, "", "Pas d'adresse de media en Slot 4.\r\nVeuillez entrer une adresse de media valide.");
    ui->dl4Btn->setEnabled(true);
    ui->dl4LE->setReadOnly(false);
    ui->rbHD1280_4->setEnabled(true);
    ui->rbW854_4->setEnabled(true);
    ui->rbW640_4->setEnabled(true);
    ui->rbAudioO_4->setEnabled(true);
    ui->rbVideoO_4->setEnabled(true);
    QFile mydl4ProcessFile(filenameProc4);
    if (mydl4ProcessFile.exists()) mydl4ProcessFile.remove();//Pour effacer le fichier précédent
}

// ////// Routines communes
//
//
void MainWindow::rwIniFile()
{
    QString myInifileTitle = QDir::homePath();
    myInifileTitle.append("/myVideoDL.ini");
    QFile myIniFile(myInifileTitle);
    if (myIniFile.exists())
    {
        if (myIniFile.open(QIODevice::ReadOnly))
        {
            QTextStream infile(&myIniFile);
            ui->destiLE1->setText(infile.readLine());
            ui->destiLE2->setText(infile.readLine());
            ui->destiLE3->setText(infile.readLine());
            ui->destiLE4->setText(infile.readLine());
            myIniFile.close();
        }
    }
    else
    {
        if (myIniFile.open(QIODevice::WriteOnly))
        {
            QTextStream outfile(&myIniFile);
            outfile << QDir::homePath() << endl;
            outfile << QDir::homePath() << endl;
            outfile << QDir::homePath() << endl;
            outfile << QDir::homePath() << endl;
            ui->destiLE1->setText(QDir::homePath());
            ui->destiLE2->setText(QDir::homePath());
            ui->destiLE3->setText(QDir::homePath());
            ui->destiLE4->setText(QDir::homePath());
            myIniFile.close();
        }
    }
    lastPath1 = ui->destiLE1->text();
    lastPath2 = ui->destiLE2->text();
    lastPath3 = ui->destiLE3->text();
    lastPath4 = ui->destiLE4->text();
}
void MainWindow::updateIniFile()
{
    QString myInifileTitle = QDir::homePath();
    myInifileTitle.append("/myVideoDL.ini");
    QFile myIniFile(myInifileTitle);
    if (myIniFile.open(QIODevice::WriteOnly))
    {
        QTextStream outfile(&myIniFile);
        outfile << ui->destiLE1->text() << endl;
        outfile << ui->destiLE2->text() << endl;
        outfile << ui->destiLE3->text() << endl;
        outfile << ui->destiLE4->text() << endl;
        myIniFile.close();
    }
    lastPath1 = ui->destiLE1->text();
    lastPath2 = ui->destiLE2->text();
    lastPath3 = ui->destiLE3->text();
    lastPath4 = ui->destiLE4->text();
}
void MainWindow::updateAppSize()
{
    //On lit ici le fichier ../myProgs/zum.directories
    QString myZumFile = QDir::homePath();
    myZumFile.append("/myProgs/zum.directories");
    QFile myZIniFile(myZumFile);
    if (myZIniFile.exists())
    {
        if (myZIniFile.open(QIODevice::ReadOnly))
        {
            QTextStream infile(&myZIniFile);
            zumRoot = infile.readLine();
            zumRoot = zumRoot.left(zumRoot.length()-4);//On ne garde que la racine du stockage de zum, sans "/Zic"
            myZIniFile.close();
            ui->centralWidget->setGeometry(0, 0, 801, 591);
            ui->centralWidget->setMinimumWidth(801);
            ui->centralWidget->setMaximumWidth(801);
            this->setMinimumWidth(801);//On pointe le MainWindow visible dans creator (le contenant le plus haut) par this->, et non pas par ui->MainWindow->...
            this->setMaximumWidth(801);
        }
    }
    else
    {
        zumRoot = "noZum";
        ui->centralWidget->setGeometry(0, 0, 459, 591);
        ui->centralWidget->setMinimumWidth(459);
        ui->centralWidget->setMaximumWidth(459);
        this->setMinimumWidth(459);
        this->setMaximumWidth(459);
    }
}
void MainWindow::on_setPath1Btn_clicked()
{
    QString myDir1 = QFileDialog::getExistingDirectory();
    ui->destiLE1->setText(myDir1);
    updateIniFile();
}
void MainWindow::on_setPath2Btn_clicked()
{
    QString myDir1 = QFileDialog::getExistingDirectory();
    ui->destiLE2->setText(myDir1);
    updateIniFile();
}
void MainWindow::on_setPath3Btn_clicked()
{
    QString myDir1 = QFileDialog::getExistingDirectory();
    ui->destiLE3->setText(myDir1);
    updateIniFile();
}
void MainWindow::on_setPath4Btn_clicked()
{
    QString myDir1 = QFileDialog::getExistingDirectory();
    ui->destiLE4->setText(myDir1);
    updateIniFile();
}

void MainWindow::on_zum1GB_clicked(bool checked)
{
    ui->zum1ArtistTitleLE->setEnabled(checked);
    ui->zum1SubtitleAlbumLE->setEnabled(checked);
}
void MainWindow::on_zum2GB_clicked(bool checked)
{
    ui->zum2ArtistTitleLE->setEnabled(checked);
    ui->zum2SubtitleAlbumLE->setEnabled(checked);
}
void MainWindow::on_zum3GB_clicked(bool checked)
{
    ui->zum3ArtistTitleLE->setEnabled(checked);
    ui->zum3SubtitleAlbumLE->setEnabled(checked);
}
void MainWindow::on_zum4GB_clicked(bool checked)
{
    ui->zum4ArtistTitleLE->setEnabled(checked);
    ui->zum4SubtitleAlbumLE->setEnabled(checked);
}
