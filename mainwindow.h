#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProcess>
#include <QTimer>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    // ////// SLOT 1
    //
    void on_dl1Btn_clicked();
    void dlFormatProcess1ReadFromProcess();
    void dlFormatProcess1Finished(int oneExitCode);
    void dlProcess1ReadFromProcess();
    void dlProcess1Finished(int oneExitCode);

    // ////// SLOT 2
    //
    void on_dl2Btn_clicked();
    void dlFormatProcess2ReadFromProcess();
    void dlFormatProcess2Finished(int oneExitCode);
    void dlProcess2ReadFromProcess();
    void dlProcess2Finished(int oneExitCode);

    // ////// SLOT 3
    //
    void on_dl3Btn_clicked();
    void dlFormatProcess3ReadFromProcess();
    void dlFormatProcess3Finished(int oneExitCode);
    void dlProcess3ReadFromProcess();
    void dlProcess3Finished(int oneExitCode);

    // ////// SLOT 4
    //
    void on_dl4Btn_clicked();
    void dlFormatProcess4ReadFromProcess();
    void dlFormatProcess4Finished(int oneExitCode);
    void dlProcess4ReadFromProcess();
    void dlProcess4Finished(int oneExitCode);


    // ////// Routines communes
    //
    void rwIniFile();
    void updateIniFile();
    void updateAppSize();

    void on_setPath1Btn_clicked();
    void on_setPath2Btn_clicked();
    void on_setPath3Btn_clicked();
    void on_setPath4Btn_clicked();

    void on_zum1GB_clicked(bool checked);
    void on_zum2GB_clicked(bool checked);
    void on_zum3GB_clicked(bool checked);
    void on_zum4GB_clicked(bool checked);

private:
    Ui::MainWindow *ui;

    QString lastPath1;
    QString lastPath2;
    QString lastPath3;
    QString lastPath4;

    QTimer *updateGuiTimer;
    QString zumRoot;

    QString filenameFormatProc1;
    QString filenameFormatProc2;
    QString filenameFormatProc3;
    QString filenameFormatProc4;

    QString filenameProc1;
    QString filenameProc2;
    QString filenameProc3;
    QString filenameProc4;

    QProcess dlFormatProcess1;
    QProcess dlFormatProcess2;
    QProcess dlFormatProcess3;
    QProcess dlFormatProcess4;

    QStringList dlFormatList1;
    QStringList dlFormatList2;
    QStringList dlFormatList3;
    QStringList dlFormatList4;

    QProcess dlProcess1;
    QProcess dlProcess2;
    QProcess dlProcess3;
    QProcess dlProcess4;
};

#endif // MAINWINDOW_H
